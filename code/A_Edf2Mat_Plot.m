% Plot the raw .edf data from each run.

% 170914 | JQK created script

%% set up paths

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/';
pn.plotFolder   = [pn.root, 'A_preprocessing/C_figures/A_Edf2Mat/'];
pn.tools        = [pn.root, 'A_preprocessing/D_tools/']; addpath(genpath(pn.tools));
pn.edf_IN       = [pn.root, 'A_preprocessing/B_data/A_eye_edf/'];

%% Loop through subjects and load .edf files

fileNames = dir([pn.edf_IN, 'S1*.edf']);
fileNames = {fileNames().name}';

for indID = 1:numel(fileNames)
    try
        edfFile = [pn.edf_IN, fileNames{indID}];
        edf0 = Edf2Mat(edfFile);
        % plot information
        plot(edf0);
        %saveas(gcf, [pn.plotFolder, fileNames{indID}(1:end-4), '_Edf2Mat'], 'fig');
        saveas(gcf, [pn.plotFolder, fileNames{indID}(1:end-4), '_Edf2Mat'], 'epsc');
        saveas(gcf, [pn.plotFolder, fileNames{indID}(1:end-4), '_Edf2Mat'], 'png');
        close all;
    catch
        disp(['Error for subject ', fileNames{indID}]);
    end
end % subject loop