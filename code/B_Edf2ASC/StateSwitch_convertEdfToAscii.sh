#!/bin/bash

# move to edf2asc directory
cd /Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/A_scripts/B_Edf2ASC/

# convert .edf-files to ascii
files=$(ls /Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/A_eye_edf/S2*.edf) # get the individual files and count the number of Ss
for fileName in $files; do
	./edf2asc -vel -res -input $fileName
done

# move files
mv /Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/A_eye_edf/*.asc /Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/A_preprocessing/B_data/B_eye_ascii/
