[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Preprocessing for eye-tracking data

Convert the original .edf recordings into ascii and mat format.

## Notes

The following data are missing:
1223: runs 2-4
1228: run 4